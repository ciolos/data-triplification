import csv
import sys
import time
from rdflib import Graph, Literal, Namespace, RDF, URIRef
from rdflib.namespace import XSD, RDFS

# Load the SAREF ontology
SAREF = Namespace("https://saref.etsi.org/core/")
SAREF_UNIT = Namespace("https://saref.etsi.org/saref#Unit")

def create_rdf_graph(input_csv):
    # Initialize a graph
    g = Graph()
    g.bind("saref", SAREF)

    print("Reading the CSV file...")
    with open(input_csv, 'r') as file:
        reader = csv.DictReader(file)
        row_count = sum(1 for row in reader)
        file.seek(0)
        next(reader)  # Skip header
        print(f"Total rows to process: {row_count}")

        for i, row in enumerate(reader, 1):
            if i % 100 == 0:
                print(f"Processing row {i}/{row_count}...")

            timestamp = row["utc_timestamp"]

            for device_id, value in row.items():
                if device_id in ["utc_timestamp", "cet_cest_timestamp", "interpolated"] or value == '':
                    continue

                # Create unique URIs for each device and measurement
                device_uri = URIRef(f"http://example.org/device/{device_id}")
                measurement_uri = URIRef(f"http://example.org/measurement/{device_id}/{timestamp}")

                # Add triples to the graph
                g.add((device_uri, RDF.type, SAREF.Device))
                g.add((measurement_uri, RDF.type, SAREF.Measurement))
                g.add((measurement_uri, SAREF.hasTimestamp, Literal(timestamp, datatype=XSD.dateTime)))
                g.add((measurement_uri, SAREF.hasValue, Literal(value, datatype=XSD.float)))
                g.add((measurement_uri, SAREF.isMeasurementOf, device_uri))
                
                # Add unit of measurement (assuming the unit is kilowatt-hour for this example)
                unit_uri = URIRef("http://example.org/unit/kWh")
                g.add((measurement_uri, SAREF.hasUnit, unit_uri))
                g.add((unit_uri, RDF.type, SAREF_UNIT.UnitOfMeasurement))
                g.add((unit_uri, RDFS.label, Literal("kilowatt-hour")))

    print("Finished processing rows.")
    return g

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python transformer.py path/to/dataset.csv")
        sys.exit(1)

    input_csv = sys.argv[1]

    start_time = time.time()

    print("Starting RDF graph creation...")
    rdf_graph = create_rdf_graph(input_csv)

    creation_end_time = time.time()

    print(f"RDF graph contains {len(rdf_graph)} triples.")
    
    if len(rdf_graph) == 0:
        print("Warning: The RDF graph is empty. Please check the input data and transformation logic.")

    print("Serializing the RDF graph to Turtle format...")
    serialization_start_time = time.time()
    rdf_graph.serialize(destination="graph.ttl", format="turtle")
    serialization_end_time = time.time()

    end_time = time.time()

    print(f"RDF graph creation time: {creation_end_time - start_time} seconds")
    print(f"Serialization time: {serialization_end_time - serialization_start_time} seconds")
    print(f"Total execution time: {end_time - start_time} seconds")
